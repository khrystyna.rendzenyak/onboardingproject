// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;
using System.Collections.Generic;

public class OnboardingProjectServerTarget : TargetRules //Change this line according to the name of your project
{
	public OnboardingProjectServerTarget(TargetInfo Target) : base(Target) //Change this line according to the name of your project
	{
		Type = TargetType.Server;
		DefaultBuildSettings = BuildSettingsVersion.V2;
		ExtraModuleNames.Add("OnboardingProject"); //Change this line according to the name of your project
	}
}