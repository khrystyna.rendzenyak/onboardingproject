// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class OnboardingProject : ModuleRules
{
	public OnboardingProject(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] 
		{ 
			"Core", 
			"CoreUObject", 
			"Engine", 
			"InputCore", 
			"HeadMountedDisplay", 
			"Niagara",
			"Sockets",
			"SlateCore",
			"OnlineSubsystem",
			"OnlineSubsystemSteam",
			"MultiplayerSession"
		});

		PublicIncludePaths.AddRange(new string[]
		{
			"OnboardingProject/Character",
			"OnboardingProject/Game",
			"OnboardingProject/Persistence",
			"OnboardingProject/Player",
			"OnboardingProject/UI",
			"OnboardingProject/World",
			"OnboardingProject/World/Pickups",
			"OnboardingProject"
		});
	}
}
