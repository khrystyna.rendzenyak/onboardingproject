// Copyright Epic Games, Inc. All Rights Reserved.

#include "OnboardingProject.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, OnboardingProject, "OnboardingProject" );
 