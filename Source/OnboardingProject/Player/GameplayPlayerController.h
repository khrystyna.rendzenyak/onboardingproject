// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "GameplayPlayerController.generated.h"

class AOnboardingProjectCharacter;

UCLASS(config = Game)
class ONBOARDINGPROJECT_API AGameplayPlayerController : public APlayerController
{
	GENERATED_BODY()

private:
	AOnboardingProjectCharacter* PlayerCharacter;

protected:
	static AGameplayPlayerController* CachedPlayerController;

	void CashePlayerController();
	void ClearCachePlayerController();

	virtual void BeginPlay() override;

public:
	AOnboardingProjectCharacter* GetCharacter() const { return PlayerCharacter;  }

	UFUNCTION(BlueprintPure, Category = "Player Controller", meta = (UnsafeDuringActorConstruction = "true"))
	static AGameplayPlayerController* GetGameplayPlayerController() { return CachedPlayerController; }

	virtual void OnPossess(APawn* aPawn) override;
	virtual void OnUnPossess() override;


};
