// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "LobbyPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class ONBOARDINGPROJECT_API ALobbyPlayerController : public APlayerController
{
	GENERATED_BODY()

private:
	UPROPERTY(ReplicatedUsing = OnRep_GameplayStateChanged)
	bool isGameplayState = false;

protected:
	virtual void BeginPlay() override;
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

public:
	UFUNCTION()
	void SetGameplayFlag(bool GameplayState);

	UFUNCTION()
	bool GetGameplayFlag() const;

	UFUNCTION(Server, Unreliable, WithValidation)
	void OnRep_GameplayStateChanged();
	void OnRep_GameplayStateChanged_Implementation();
	bool OnRep_GameplayStateChanged_Validate();
};
