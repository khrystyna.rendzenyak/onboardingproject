// Fill out your copyright notice in the Description page of Project Settings.


#include "GameplayPlayerController.h"
#include "Character/OnboardingProjectCharacter.h"

AGameplayPlayerController* AGameplayPlayerController::CachedPlayerController = nullptr;

void AGameplayPlayerController::BeginPlay()
{
	Super::BeginPlay();

	SetInputMode(FInputModeGameAndUI());
	PlayerCharacter = nullptr;
}

void AGameplayPlayerController::CashePlayerController()
{
	if (!IsPendingKill() && GetWorld() && GetWorld()->IsGameWorld())
	{
		if (CachedPlayerController == nullptr)
		{
			CachedPlayerController = this;
		}
	}
}

void AGameplayPlayerController::ClearCachePlayerController()
{
	if (CachedPlayerController != nullptr)
	{
		if (CachedPlayerController == this)
		{
			CachedPlayerController = nullptr;
		}
	}
}

void AGameplayPlayerController::OnPossess(APawn* aPawn)
{
	CashePlayerController();

	Super::OnPossess(aPawn);

	PlayerCharacter = Cast<AOnboardingProjectCharacter>(aPawn);
}

void AGameplayPlayerController::OnUnPossess()
{
	ClearCachePlayerController();
	if (PlayerCharacter)
	{
		PlayerCharacter->PrimaryActorTick.RemovePrerequisite(this, PrimaryActorTick);
	}
	PlayerCharacter = nullptr;

	Super::OnUnPossess();
}