// Fill out your copyright notice in the Description page of Project Settings.


#include "Player/LobbyPlayerController.h"
#include "OnboardingProjectGlobals.h"
#include "Net/UnrealNetwork.h"

//#include "LobbyGameMode.h"
//#include "Kismet/GameplayStatics.h"	
//#include "Blueprint/WidgetBlueprintLibrary.h"

DEFINE_LOG_CATEGORY_STATIC(LogLobbyPC, All, All);

void ALobbyPlayerController::BeginPlay()
{
	Super::BeginPlay();

	SetInputMode(FInputModeGameAndUI());
	bShowMouseCursor = true;
}


void ALobbyPlayerController::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(ALobbyPlayerController, isGameplayState);
}

void ALobbyPlayerController::SetGameplayFlag(bool GameplayState)
{
	isGameplayState = GameplayState;
	UE_LOG(LogLobbyPC, Display, TEXT("SetGameplayFlag function"));
	OnRep_GameplayStateChanged();
}

bool ALobbyPlayerController::GetGameplayFlag() const
{
	return isGameplayState;
}

//void ALobbyPlayerController::OnRep_GameplayStateChanged()
//{
//	UE_LOG(LogLobbyPC, Display, TEXT("OnRep_GameplayStateChanged function"));
//	
//	if (HasAuthority())
//	{
//		GetWorld()->ServerTravel(GetGlobals().GetMapsPath() + GetGlobals().StartupLevelName.ToString()+TEXT("?listen"));
//	}
//}

void ALobbyPlayerController::OnRep_GameplayStateChanged_Implementation()
{
	UE_LOG(LogLobbyPC, Display, TEXT("OnRep_GameplayStateChanged function"));

	if (HasAuthority())
	{
		GetWorld()->ServerTravel(GetGlobals().GetMapsPath() + GetGlobals().StartupLevelName.ToString() + TEXT("?listen"));
	}
}

bool ALobbyPlayerController::OnRep_GameplayStateChanged_Validate()
{
	return true;
}
