// Fill out your copyright notice in the Description page of Project Settings.


#include "LeaderboardPlayerController.h"

void ALeaderboardPlayerController::BeginPlay()
{
	Super::BeginPlay();

	SetInputMode(FInputModeUIOnly());
	bShowMouseCursor = true;
}