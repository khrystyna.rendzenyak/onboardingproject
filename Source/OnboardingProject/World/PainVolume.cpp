// Fill out your copyright notice in the Description page of Project Settings.


#include "PainVolume.h"
#include "DrawDebugHelpers.h"
#include "Kismet/GameplayStatics.h"
#include "OnboardingProjectUtils.h"
#include "OnboardingProjectGlobals.h"

APainVolume::APainVolume()
{
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>("SceneComponent");
	SetRootComponent(SceneComponent);
}

void APainVolume::BeginPlay()
{
	Super::BeginPlay();

	Damage = GetGlobals().Damage;
	GetWorldTimerManager().SetTimer(DamageTimerHandle, this, &APainVolume::DamagePlayer, GetGlobals().DamageTimer, true);
	GetWorldTimerManager().SetTimer(DamageMultTimerHandle, this, &APainVolume::IncreaseDamage, GetGlobals().IncreaseDamageTimer, true);
}

void APainVolume::DamagePlayer()
{
	if (HasAuthority())
	{
		UGameplayStatics::ApplyRadialDamage(GetWorld(), Damage, GetActorLocation(), Radius, nullptr, {}, this, nullptr, true);
	}
}

void APainVolume::IncreaseDamage()
{
	Damage *= GetGlobals().DamageMultiplier;
}

void APainVolume::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	DrawDebugSphere(GetWorld(), GetActorLocation(), Radius, 24, SphereColor);
}