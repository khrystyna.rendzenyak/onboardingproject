// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "OnboardingProjectCharacter.h"
#include "JumpPlatform.generated.h"

UCLASS()
class ONBOARDINGPROJECT_API AJumpPlatform : public AActor
{
	GENERATED_BODY()
	
public:	
	AJumpPlatform();

protected:
	virtual void BeginPlay() override;

	UFUNCTION()
	virtual void OnBoxOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

private:
	UPROPERTY(EditAnywhere)
	class UBoxComponent* OverlapBox;

	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* JumpMesh;

	UPROPERTY(EditAnywhere)
	float JumpZVelocity = 100.f;

	UPROPERTY(EditAnywhere)
	AOnboardingProjectCharacter* UserCharacter;

	void Jump();
};
