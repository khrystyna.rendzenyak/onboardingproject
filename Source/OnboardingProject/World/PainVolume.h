// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PainVolume.generated.h"

UCLASS()
class ONBOARDINGPROJECT_API APainVolume : public AActor
{
	GENERATED_BODY()
	
public:	
	APainVolume();

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	USceneComponent* SceneComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Radius = 5000.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FColor SphereColor = FColor::Red;

	virtual void Tick(float DeltaTime) override;

protected:
	virtual void BeginPlay() override;

private:
	UPROPERTY()
	float Damage;

	FTimerHandle DamageTimerHandle;
	FTimerHandle DamageMultTimerHandle;

	void DamagePlayer();
	void IncreaseDamage();
};
