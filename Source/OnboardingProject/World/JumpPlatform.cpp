// Fill out your copyright notice in the Description page of Project Settings.


#include "JumpPlatform.h"
#include "Components/BoxComponent.h"
#include "GameFramework/CharacterMovementComponent.h"

AJumpPlatform::AJumpPlatform()
{
	PrimaryActorTick.bCanEverTick = true;
	bReplicates = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));

	OverlapBox = CreateDefaultSubobject<UBoxComponent>(TEXT("OverlapBox"));
	OverlapBox->SetupAttachment(RootComponent);
	OverlapBox->SetBoxExtent(FVector(50.f, 50.f, 50.f));
	OverlapBox->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	OverlapBox->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	OverlapBox->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Overlap);

	JumpMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("JumpMesh"));
	JumpMesh->SetupAttachment(OverlapBox);
	JumpMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);

}

void AJumpPlatform::BeginPlay()
{
	Super::BeginPlay();
	
	OverlapBox->OnComponentBeginOverlap.AddDynamic(this, &AJumpPlatform::OnBoxOverlap);
}

void AJumpPlatform::OnBoxOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor)
	{
		UserCharacter = Cast<AOnboardingProjectCharacter>(OtherActor);
		Jump();
	}
}

void AJumpPlatform::Jump()
{
	UCharacterMovementComponent* CharacterMovement = UserCharacter ? UserCharacter->GetCharacterMovement() : nullptr;
	if (CharacterMovement)
	{
		CharacterMovement->JumpZVelocity = JumpZVelocity;
		CharacterMovement->PendingLaunchVelocity.Z = JumpZVelocity;
	}
}