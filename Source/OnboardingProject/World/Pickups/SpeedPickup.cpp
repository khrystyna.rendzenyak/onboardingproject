// Fill out your copyright notice in the Description page of Project Settings.


#include "SpeedPickup.h"
#include "HealthComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Components/TextRenderComponent.h"
#include "Components/WidgetComponent.h"
#include "RespawnTimerWidget.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "OnboardingProjectUtils.h"
#include "OnboardingProjectGlobals.h"
#include "Net/UnrealNetwork.h"

DEFINE_LOG_CATEGORY_STATIC(LogSpeedPickup, All, All);

ASpeedPickup::ASpeedPickup()
{
	bReplicates = true;

	TimerComponent = CreateDefaultSubobject<UWidgetComponent>("TimerComponent");
	TimerComponent->SetupAttachment(GetRootComponent());
	isTimerVisible = false;
	TimerComponent->SetVisibility(isTimerVisible, true);
	TimerComponent->SetWidgetSpace(EWidgetSpace::Screen);
}

void ASpeedPickup::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ASpeedPickup, isTimerVisible); 
	DOREPLIFETIME(ASpeedPickup, TimeToRespawn);
}

void ASpeedPickup::OnRep_SetVisibility()
{
	if (isTimerVisible)
	{
		TimerComponent->SetVisibility(true, true);
		if (PickupState != EPickupState::EPS_Taken)
		{
			PickupState = EPickupState::EPS_Taken;
			OnRep_PickupStateChanged();
		}
	}
	else
	{
		TimerComponent->SetVisibility(false, true);
		if (PickupState == EPickupState::EPS_Taken)
		{
			PickupState = EPickupState::EPS_Idle;
			OnRep_PickupStateChanged();
		}
	}
}

void ASpeedPickup::OnRep_UpdateTimerWidget()
{
	URespawnTimerWidget* TimerWidget = Cast<URespawnTimerWidget>(TimerComponent->GetUserWidgetObject());
	if (TimerWidget)
	{
		TimerWidget->SetTimerText(TimeToRespawn);
	}
}

bool ASpeedPickup::GivePickupTo()
{
	if (!UserCharacter)
	{
		return false;
	}

	UCharacterMovementComponent* CharacterMovement = UserCharacter->GetCharacterMovement();
	if(!CharacterMovement)
	{
		return false;
	}

	UE_LOG(LogSpeedPickup, Display, TEXT("Speed was taken"));
	InitialBaseSpeed = CharacterMovement->MaxWalkSpeed;
	InitialCrouchSpeed = CharacterMovement->MaxWalkSpeedCrouched;

	GetWorldTimerManager().SetTimer(SpeedTimerHandle, this, &ASpeedPickup::ResetSpeeds, GetGlobals().SpeedTime);
	GetWorldTimerManager().SetTimer(RespawnTimerHandle, this, &ASpeedPickup::Respawn, GetGlobals().RespawnTime);
	GetWorldTimerManager().SetTimer(TimeToRespawnTimerHandle, this, &ASpeedPickup::UpdateTimer, UpdateTimerTime, true);
	GetWorldTimerManager().SetTimer(RotateTimerHandle, this, &ASpeedPickup::UpdateTimerRotation, UpdateRotationTime, true);

	TimeToRespawn = GetGlobals().RespawnTime;
	isTimerVisible = true;
	TimerComponent->SetVisibility(isTimerVisible, true);
	URespawnTimerWidget* TimerWidget = Cast<URespawnTimerWidget>(TimerComponent->GetUserWidgetObject());
	if (!TimerWidget) return false;
	TimerWidget->SetTimerText(TimeToRespawn);
	
	float SpeedMult = GetGlobals().SpeedMultiplier;

	CharacterMovement->MaxWalkSpeed = InitialBaseSpeed * SpeedMult;
	CharacterMovement->MaxWalkSpeedCrouched = InitialCrouchSpeed * SpeedMult;
	return true;
}

void ASpeedPickup::Respawn()
{
	isTimerVisible = false;
	TimerComponent->SetVisibility(isTimerVisible, true);
	GetWorldTimerManager().ClearTimer(TimeToRespawnTimerHandle);
	GetWorldTimerManager().ClearTimer(RotateTimerHandle);
	
	Super::Respawn();
}

void ASpeedPickup::ResetSpeeds()
{
	UCharacterMovementComponent* CharacterMovement = UserCharacter ? UserCharacter->GetCharacterMovement() : nullptr;
	if (CharacterMovement)
	{
		CharacterMovement->MaxWalkSpeed = InitialBaseSpeed;
		CharacterMovement->MaxWalkSpeedCrouched = InitialCrouchSpeed;
	}
}

void ASpeedPickup::UpdateTimer()
{
	TimeToRespawn--; 
	OnRep_UpdateTimerWidget();
}

void ASpeedPickup::UpdateTimerRotation()
{
	if (UserCharacter)
	{
		FRotator NewRot = FMath::RInterpTo(UserCharacter->GetActorRotation(), UKismetMathLibrary::FindLookAtRotation(this->GetActorLocation(), UserCharacter->GetActorLocation()), 0.0f, 0.0f);
		SetActorRotation(NewRot);
	}
}