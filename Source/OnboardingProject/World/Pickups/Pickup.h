// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "OnboardingProjectCharacter.h"
#include "Pickup.generated.h"

class AOnboardingProjectCharacter;

UENUM(BlueprintType)
enum class EPickupState : uint8
{
	EPS_Hidden UMETA(DisplayName = "Hidden"),
	EPS_Idle UMETA(DisplayName = "Idle"),
	EPS_Taken UMETA(DisplayName = "Taken"),

	EPS_MAX UMETA(DisplayName = "DefaultMax")
};

UCLASS()
class ONBOARDINGPROJECT_API APickup : public AActor
{
	GENERATED_BODY()
	
public:	
	APickup();

	UFUNCTION()
	virtual void Respawn();

protected:
	virtual void BeginPlay() override;
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	UPROPERTY()
	AOnboardingProjectCharacter* UserCharacter;

	UPROPERTY(ReplicatedUsing = OnRep_PickupStateChanged, VisibleAnywhere)
	EPickupState PickupState;

	UFUNCTION()
	virtual void OnSphereOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
		
	UFUNCTION()
	virtual void PickupWasTaken();

	UFUNCTION()
	void OnRep_PickupStateChanged();

	virtual void HidePickup();

private:
	UPROPERTY(EditAnywhere)
	class USphereComponent* OverlapSphere;

	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* BasePickupMesh;

	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* PickupMesh;

	UPROPERTY(VisibleAnywhere)
	class UNiagaraComponent* PickupEffectComponent;

	UPROPERTY(EditAnywhere)
	class UNiagaraSystem* PickupEffect;

	UFUNCTION()
	virtual bool GivePickupTo();
};
