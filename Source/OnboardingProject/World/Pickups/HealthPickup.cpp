// Fill out your copyright notice in the Description page of Project Settings.


#include "HealthPickup.h"
#include "HealthComponent.h"
#include "OnboardingProjectUtils.h"
#include "OnboardingProjectGameMode.h"

DEFINE_LOG_CATEGORY_STATIC(LogHealthPickup, All, All);

AHealthPickup::AHealthPickup()
{
	bReplicates = true;
}

void AHealthPickup::BeginPlay()
{
	Super::BeginPlay();

	HidePickup();
}

bool AHealthPickup::GivePickupTo()
{
	UE_LOG(LogHealthPickup, Display, TEXT("Health was taken"));

	if (!UserCharacter)
	{
		return false;
	}
	UHealthComponent* HealthComponent = Cast<UHealthComponent>(UserCharacter->GetComponentByClass(UHealthComponent::StaticClass()));
	if (!HealthComponent)
	{
		return false;
	}
	return HealthComponent->TryToAddHealth(HealAmount);
}

void AHealthPickup::PickupWasTaken()
{
	Super::PickupWasTaken();
	
	AOnboardingProjectGameMode* GameMode = OnboardingProjectUtils::GetGameMode(this); 
	if (GameMode)
	{
		GameMode->SpawnHealthPickup();
	}
}