// Fill out your copyright notice in the Description page of Project Settings.


#include "Pickup.h"
#include "Components/SphereComponent.h"
#include "NiagaraFunctionLibrary.h"
#include "NiagaraComponent.h"
#include "Net/UnrealNetwork.h"

APickup::APickup()
{
	bReplicates = true;
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));

	OverlapSphere = CreateDefaultSubobject<USphereComponent>(TEXT("OverlapSphere"));
	OverlapSphere->SetupAttachment(RootComponent);
	OverlapSphere->SetSphereRadius(150.f);
	OverlapSphere->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	OverlapSphere->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	OverlapSphere->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Overlap);

	PickupMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("PickupMesh"));
	PickupMesh->SetupAttachment(OverlapSphere);
	PickupMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	BasePickupMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BasePickupMesh"));
	BasePickupMesh->SetupAttachment(OverlapSphere);
	BasePickupMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	PickupEffectComponent = CreateDefaultSubobject<UNiagaraComponent>(TEXT("PickupEffectComponent"));
	PickupEffectComponent->SetupAttachment(RootComponent);

	PickupState = EPickupState::EPS_Idle;
	OnRep_PickupStateChanged();
}

void APickup::BeginPlay()
{
	Super::BeginPlay();

	if (HasAuthority())
	{
		OverlapSphere->OnComponentBeginOverlap.AddDynamic(this, &APickup::OnSphereOverlap);
	}
}

void APickup::OnSphereOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (!OtherActor)
	{
		return;
	}
	UserCharacter = Cast<AOnboardingProjectCharacter>(OtherActor);
	if (UserCharacter && GivePickupTo())
	{
		PickupWasTaken();
	}
}

void APickup::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(APickup, PickupState);
}

bool APickup::GivePickupTo()
{
	return false;
}

void APickup::PickupWasTaken()
{
	PickupState = EPickupState::EPS_Taken;
	if (PickupEffect)
	{
		UNiagaraFunctionLibrary::SpawnSystemAtLocation(this, PickupEffect, GetActorLocation(), GetActorRotation());
	}
	HidePickup();
}

void APickup::OnRep_PickupStateChanged()
{
	switch (PickupState)
	{
	case EPickupState::EPS_Hidden:
		PickupEffectComponent->SetVisibility(false, true);
		break;
	case EPickupState::EPS_Idle:
		PickupEffectComponent->SetVisibility(true, true);
		break;
	case EPickupState::EPS_Taken:
		PickupEffectComponent->SetVisibility(false, true);
		break;
	}
}

void APickup::HidePickup()
{
	if (HasAuthority())
	{
		OverlapSphere->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
		PickupState = EPickupState::EPS_Hidden; 
		OnRep_PickupStateChanged();
	}
}

void APickup::Respawn()
{
	if (HasAuthority())
	{
		PickupState = EPickupState::EPS_Idle; 
		OnRep_PickupStateChanged();
		OverlapSphere->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Overlap);
	}
}