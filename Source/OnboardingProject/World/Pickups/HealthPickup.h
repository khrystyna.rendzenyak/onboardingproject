// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Pickup.h"
#include "HealthPickup.generated.h"

/**
 * 
 */
UCLASS()
class ONBOARDINGPROJECT_API AHealthPickup : public APickup
{
	GENERATED_BODY()

public:
	AHealthPickup();

protected:
	UPROPERTY(EditAnywhere)
	float HealAmount = 100.f;

	virtual void BeginPlay() override;

private:
	virtual bool GivePickupTo() override;
	virtual void PickupWasTaken() override;
};
