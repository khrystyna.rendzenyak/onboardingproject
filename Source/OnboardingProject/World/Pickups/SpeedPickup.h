// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Pickup.h"
#include "SpeedPickup.generated.h"

class UTextRenderComponent;
class UWidgetComponent;

UCLASS()
class ONBOARDINGPROJECT_API ASpeedPickup : public APickup
{
	GENERATED_BODY()
public:
	ASpeedPickup();

	virtual void Respawn() override;

protected:
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

private:
	UPROPERTY(VisibleAnywhere)
	UWidgetComponent* TimerComponent;

	UPROPERTY(ReplicatedUsing = OnRep_SetVisibility)
	bool isTimerVisible;

	UPROPERTY(ReplicatedUsing = OnRep_UpdateTimerWidget)
	int32 TimeToRespawn;

	UFUNCTION()
	void UpdateTimer();

	UFUNCTION()
	void OnRep_SetVisibility();

	UFUNCTION()
	void OnRep_UpdateTimerWidget();

	float InitialBaseSpeed;
	float InitialCrouchSpeed;
	float UpdateTimerTime = 1.f;
	float UpdateRotationTime = .2f;

	virtual bool GivePickupTo() override;

	void ResetSpeeds();
	void UpdateTimerRotation();

	FTimerHandle SpeedTimerHandle;
	FTimerHandle RespawnTimerHandle;
	FTimerHandle TimeToRespawnTimerHandle;
	FTimerHandle RotateTimerHandle;
};
