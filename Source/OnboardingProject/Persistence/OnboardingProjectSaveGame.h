// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"
#include "Containers/Map.h"
#include "OnboardingProjectSaveGame.generated.h"

USTRUCT()
struct FSaveData
{
	GENERATED_BODY()

	FSaveData(int32 CurrentTime, FString Name) : SaveDateTime(CurrentTime), PlayerCharacterName(Name) {}
	FSaveData()=default;

	UPROPERTY()
	FString PlayerCharacterName;
	UPROPERTY()
	FDateTime SaveDateTime;
	UPROPERTY()
	int32 UserPlayTime;
};

UCLASS()
class ONBOARDINGPROJECT_API UOnboardingProjectSaveGame : public USaveGame
{
	GENERATED_BODY()
public:
	friend class UOnboardingProjectGameInstance;

	UOnboardingProjectSaveGame();

	virtual void Init();
	virtual void Shutdown();
	virtual void Reset();

	UFUNCTION(exec, Category = "Save/Load")
	virtual void SaveGame();

	void OnPreLoadMap(const FString& MapName);
	void OnPostLoadMapName(UWorld* World);

	void SetCurrentUserName(FString UserName) { CurrentUserName = UserName; }
	FString GetCurrentUserName() const { return CurrentUserName; }
	void GetUserNames(TArray<FString>& OutUserNames);
	void SetUserData(int32 Time);
	TArray<FSaveData> GetUserSaveData() const { return UsersSaveData; }
private:
	UOnboardingProjectSaveGame* CurrentSaveGame;

	UFUNCTION(exec, Category = "Save/Load")
	virtual void LoadGame();
	

	UPROPERTY()
	FString CurrentUserName;

	UPROPERTY()
	FSaveData CurrentUserSaveData;

	UPROPERTY()
	TArray<FSaveData> UsersSaveData;
};
