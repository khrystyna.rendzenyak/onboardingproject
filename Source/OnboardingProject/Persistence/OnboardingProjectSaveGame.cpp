// Fill out your copyright notice in the Description page of Project Settings.


#include "OnboardingProjectSaveGame.h"
#include "Kismet/GameplayStatics.h"
#include "Misc/DateTime.h"

UOnboardingProjectSaveGame::UOnboardingProjectSaveGame()
{
	CurrentSaveGame = this;
}

void UOnboardingProjectSaveGame::Init()
{
	LoadGame();
}

void UOnboardingProjectSaveGame::Shutdown()
{
	SaveGame();
}

void UOnboardingProjectSaveGame::Reset()
{
	Shutdown();
	Init();
}

void UOnboardingProjectSaveGame::OnPreLoadMap(const FString& MapName)
{

}

void UOnboardingProjectSaveGame::OnPostLoadMapName(UWorld* World)
{
}

void UOnboardingProjectSaveGame::GetUserNames(TArray<FString>& OutUserNames)
{
	for (auto iter : UsersSaveData)
	{
		if (!OutUserNames.Contains(iter.PlayerCharacterName))
		{
			OutUserNames.Add(iter.PlayerCharacterName);
		}
	}
}

void UOnboardingProjectSaveGame::SetUserData(int32 Time)
{
	CurrentSaveGame = Cast<UOnboardingProjectSaveGame>(UGameplayStatics::LoadGameFromSlot(TEXT("OP_Slot"), 0));
	CurrentSaveGame->CurrentUserSaveData.UserPlayTime = Time;
	CurrentSaveGame->CurrentUserSaveData.PlayerCharacterName = CurrentUserName;
	CurrentSaveGame->CurrentUserSaveData.SaveDateTime = FDateTime::Now();
	CurrentSaveGame->UsersSaveData.Add(CurrentSaveGame->CurrentUserSaveData);

	CurrentUserSaveData = CurrentSaveGame->CurrentUserSaveData;
	
	UsersSaveData = CurrentSaveGame->UsersSaveData;
}

void UOnboardingProjectSaveGame::LoadGame()
{
	CurrentSaveGame = Cast<UOnboardingProjectSaveGame>(UGameplayStatics::LoadGameFromSlot(TEXT("OP_Slot"), 0));
	if (CurrentSaveGame)
	{
		CurrentUserSaveData = CurrentSaveGame->CurrentUserSaveData;
		CurrentUserName = CurrentSaveGame->CurrentUserName;
		UsersSaveData = CurrentSaveGame->UsersSaveData;
	}
}

void UOnboardingProjectSaveGame::SaveGame()
{
	UGameplayStatics::SaveGameToSlot(this, TEXT("OP_Slot"), 0);
}
