// Fill out your copyright notice in the Description page of Project Settings.


#include "LeaderboardHUD.h"

void ALeaderboardHUD::BeginPlay()
{
	Super::BeginPlay();

	if (LeaderboardWidgetClass)
	{
		const auto LeaderboardWidget = CreateWidget<UUserWidget>(GetWorld(), LeaderboardWidgetClass);
		if (LeaderboardWidget)
		{
			LeaderboardWidget->AddToViewport();
		}
	}
}