// Fill out your copyright notice in the Description page of Project Settings.


#include "LeaderboardWidget.h"
#include "Components/VerticalBox.h"
#include "Components/Button.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetStringLibrary.h"
#include "UserStatisticWidget.h"
#include "OnboardingProjectCharacter.h"
#include "OnboardingProjectGlobals.h"
#include "OnboardingProjectGameInstance.h"
#include "OnboardingProjectSaveGame.h"
#include "GenericPlatform/GenericPlatformMath.h"


void ULeaderboardWidget::NativeOnInitialized()
{
	Super::NativeOnInitialized();

	if (MainMenuButton)
	{
		MainMenuButton->OnClicked.AddDynamic(this, &ULeaderboardWidget::GoToMainMenu);
	}

	UpdateUserStat();
}

void ULeaderboardWidget::GoToMainMenu()
{
	UGameplayStatics::OpenLevel(this, GetGlobals().MenuLevelName);
}

void ULeaderboardWidget::UpdateUserStat()
{
	UOnboardingProjectSaveGame& SaveGameManager = UOnboardingProjectGameInstance::Get()->GetSaveGameManager();
	const TArray<FSaveData> playerData = SaveGameManager.GetUserSaveData();
	int32 playerDataCount = playerData.Num();
	for (int i = 0; i < FGenericPlatformMath::Min(playerDataCount, UserStatCount); i++)
	{

		const auto UserStatWidget = CreateWidget<UUserStatisticWidget>(GetWorld(), UserStatisticWidget);
		if (!UserStatWidget) continue;

		FText UserTimeText = FText::FromString(UKismetStringLibrary::TimeSecondsToString(playerData[i].UserPlayTime).Mid(0, 5));
		FText UserNameText = FText::FromString(playerData[i].PlayerCharacterName);

		UserStatWidget->SetPlayerStatistic(FText::FromString(FString::FromInt(i + 1)), UserNameText, UserTimeText);
		PlayerStatBox->AddChild(UserStatWidget);
	}
	if (playerDataCount < UserStatCount)
	{
		for (int i = playerDataCount; i < UserStatCount; i++)
		{
			const auto UserStatWidget = CreateWidget<UUserStatisticWidget>(GetWorld(), UserStatisticWidget);
			if (!UserStatWidget) continue;

			FText UserTimeText = FText::FromString("-");
			FText UserNameText = FText::FromString("");

			UserStatWidget->SetPlayerStatistic(FText::FromString(FString::FromInt(i + 1)), UserNameText, UserTimeText);
			PlayerStatBox->AddChild(UserStatWidget);
		}
	}
}
