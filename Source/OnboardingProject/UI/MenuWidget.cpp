// Fill out your copyright notice in the Description page of Project Settings.

#include "MenuWidget.h"
#include "Kismet/GameplayStatics.h"
#include "Blueprint/WidgetBlueprintLibrary.h"
#include "Game/OnboardingProjectGameInstance.h"
#include "OnboardingProjectSaveGame.h"
#include "OnboardingProjectGlobals.h"
#include "MultiplayerSessionSubsystem.h"
#include "OnboardingProjectCharacter.h"
#include "Interfaces/OnlineSessionInterface.h"

void UMenuWidget::NativeOnInitialized()
{
	Super::NativeOnInitialized();
	UOnboardingProjectSaveGame& SaveGameManager = UOnboardingProjectGameInstance::Get()->GetSaveGameManager();
	TArray<FString> UserNameArray;
	SaveGameManager.GetUserNames(UserNameArray);
	
	if (HelpText)
	{
		HelpText->SetText(FText::FromString(""));
		HelpText->SetVisibility(ESlateVisibility::Hidden);
	}
	if (NameText)
	{
		NameText->OnTextChanged.AddDynamic(this, &UMenuWidget::OnUserNameChanged);
		NameText->SetVisibility(ESlateVisibility::Hidden);

		if (UserNameArray.Num() == 0)
		{
			NameText->SetVisibility(ESlateVisibility::Visible);
			if (HelpText)
			{
				HelpText->SetVisibility(ESlateVisibility::Visible);
			}
		}
	}
	if (SavedNamesBox)
	{
		SavedNamesBox->OnSelectionChanged.AddDynamic(this, &UMenuWidget::OnSelectedSave);
		SavedNamesBox->SetVisibility(ESlateVisibility::Hidden);
		if (UserNameArray.Num() >= 1)
		{
			for (auto userName : UserNameArray)
			{
				SavedNamesBox->AddOption(userName);
			}
		}
	}
	if (CreateNewSaveButton)
	{
		if (UserNameArray.Num() >= 1)
		{
			CreateNewSaveButton->SetVisibility(ESlateVisibility::Visible);
		}
		else
		{
			CreateNewSaveButton->SetVisibility(ESlateVisibility::Hidden);
		}
		CreateNewSaveButton->OnClicked.AddDynamic(this, &UMenuWidget::OnCreateNewSave);
	}
	if (SelectGameSaveButton)
	{
		if (UserNameArray.Num() >= 1)
		{
			SelectGameSaveButton->SetVisibility(ESlateVisibility::Visible);
		}
		else
		{
			SelectGameSaveButton->SetVisibility(ESlateVisibility::Hidden);
		}
		SelectGameSaveButton->OnClicked.AddDynamic(this, &UMenuWidget::OnSelectGameSave);
	}
	if (StartGameButton)
	{
		StartGameButton->OnClicked.AddDynamic(this, &UMenuWidget::OnStartGame);
		if (NameText && NameText->Text.IsEmpty())
		{
			StartGameButton->SetIsEnabled(false);
		}
	}
	if (StartOnlineGameButton)
	{
		StartOnlineGameButton->OnClicked.AddDynamic(this, &UMenuWidget::OnStartOnlineGame);
		if (NameText && NameText->Text.IsEmpty())
		{
			StartOnlineGameButton->SetIsEnabled(false);
		}
	}
	if (LeaderboardButton)
	{
		LeaderboardButton->OnClicked.AddDynamic(this, &UMenuWidget::OnLeaderboardOpen);
	}
}
 

void UMenuWidget::OnSelectGameSave()
{
	if (CreateNewSaveButton)
	{
		CreateNewSaveButton->SetVisibility(ESlateVisibility::Hidden);
	}
	if (SelectGameSaveButton)
	{
		SelectGameSaveButton->SetVisibility(ESlateVisibility::Hidden);
	}
	if (SavedNamesBox)
	{
		SavedNamesBox->SetVisibility(ESlateVisibility::Visible);
	}
}

void UMenuWidget::OnCreateNewSave()
{
	if (CreateNewSaveButton)
	{
		CreateNewSaveButton->SetVisibility(ESlateVisibility::Hidden);
	}
	if (SelectGameSaveButton)
	{
		SelectGameSaveButton->SetVisibility(ESlateVisibility::Hidden);
	}
	if (NameText)
	{
		NameText->SetVisibility(ESlateVisibility::Visible);
	}
	if (HelpText)
	{
		HelpText->SetVisibility(ESlateVisibility::Visible);
	}
}

void UMenuWidget::OnStartGame()
{
	if (!GetWorld())
	{
		return;
	}
	UWorld* World = GetWorld();
	if (!World)
	{
		return;
	}

	UOnboardingProjectSaveGame& SaveGameManager = UOnboardingProjectGameInstance::Get()->GetSaveGameManager();
	
	if (NameText && NameText->GetVisibility() == ESlateVisibility::Visible)
	{
		SaveGameManager.SetCurrentUserName(NameText->GetText().ToString());
	}
	else if (SavedNamesBox && SavedNamesBox->GetVisibility() == ESlateVisibility::Visible)
	{
		SaveGameManager.SetCurrentUserName(SavedNamesBox->GetSelectedOption());
	}
	SaveGameManager.Shutdown();

	if (World && World->GetNetMode() == ENetMode::NM_Client && GetWorld()->GetFirstPlayerController())
	{
		FName GameplayLevel = GetGlobals().StartupLevelName;
		if (!GameplayLevel.IsNone())
		{
			FString MapPathToTravel = GetGlobals().GetMapsPath() + GameplayLevel.ToString();
			GetWorld()->GetFirstPlayerController()->ClientTravel(MapPathToTravel, TRAVEL_Absolute);
		}
	}
	else
	{
		UGameplayStatics::OpenLevel(this, GetGlobals().StartupLevelName);
	}
}

void UMenuWidget::OnStartOnlineGame()
{
	if (!GetWorld())
	{
		return;
	}
	UWorld* World = GetWorld();
	if (!World)
	{
		return;
	}

	MultiplayerSessionSubsystem = UOnboardingProjectGameInstance::Get()->GetSubsystem<UMultiplayerSessionSubsystem>();
	if (!MultiplayerSessionSubsystem)
	{
		return;
	}
	//FString LobbyPath = GetGlobals().GetMapsPath() + GetGlobals().LobbyLevelName.ToString() + TEXT("?listen");
	//MultiplayerSessionSubsystem->CreateSession(NumPublicConnections, MatchType, LobbyPath);
	
	if (World && (World->GetNetMode() != ENetMode::NM_DedicatedServer))
	{
		UOnboardingProjectSaveGame& SaveGameManager = UOnboardingProjectGameInstance::Get()->GetSaveGameManager();

		if (NameText && NameText->GetVisibility() == ESlateVisibility::Visible)
		{
			SaveGameManager.SetCurrentUserName(NameText->GetText().ToString());
		}
		else if (SavedNamesBox && SavedNamesBox->GetVisibility() == ESlateVisibility::Visible)
		{
			SaveGameManager.SetCurrentUserName(SavedNamesBox->GetSelectedOption());
		}
		SaveGameManager.Shutdown();
		
		MultiplayerSessionSubsystem->FindSessions(1000);
	}
}

void UMenuWidget::OnLeaderboardOpen()
{
	UGameplayStatics::OpenLevel(this, GetGlobals().LeaderboardLevelName);
}

void UMenuWidget::OnUserNameChanged(const FText& Text)
{
	TArray<FString> UserNameArray;
	bool isNameValid = true;

	for (auto userName : UserNameArray)
	{
		if (userName == Text.ToString())
		{
			isNameValid = false;
		}
	}

	if ((Text.ToString().Len() > 3) && isNameValid)
	{
		StartGameButton->SetIsEnabled(true);
		StartOnlineGameButton->SetIsEnabled(true);
		if (HelpText)
		{
			HelpText->SetVisibility(ESlateVisibility::Hidden);
		}
	}
	else
	{
		StartGameButton->SetIsEnabled(false);
		StartOnlineGameButton->SetIsEnabled(false);
		if (HelpText)
		{
			if (Text.ToString().Len() < 3)
			{
				HelpText->SetText(FText::FromString("This name is too short"));
			}
			else if (!isNameValid)
			{
				HelpText->SetText(FText::FromString("This name is already in use by another player"));
			}
			HelpText->SetVisibility(ESlateVisibility::Visible);
		}
	}
}

void UMenuWidget::OnSelectedSave(FString SelectedItem, ESelectInfo::Type SelectionType)
{
	if (StartGameButton)
	{
		if (SelectedItem != "")
		{
			StartGameButton->SetIsEnabled(true);
		}
		else
		{
			StartGameButton->SetIsEnabled(false);
		}
	}
	if (StartOnlineGameButton)
	{
		if (SelectedItem != "")
		{
			StartOnlineGameButton->SetIsEnabled(true);
		}
		else
		{
			StartOnlineGameButton->SetIsEnabled(false);
		}
	}
}
