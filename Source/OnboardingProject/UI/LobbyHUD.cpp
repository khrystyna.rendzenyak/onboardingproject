// Fill out your copyright notice in the Description page of Project Settings.


#include "LobbyHUD.h"
#include "Blueprint/WidgetBlueprintLibrary.h"

void ALobbyHUD::BeginPlay()
{
	Super::BeginPlay();

	if (LobbyUserWidgetClass)
	{
		const auto LobbyUserWidget = CreateWidget<UUserWidget>(GetWorld(), LobbyUserWidgetClass);
		if (LobbyUserWidget)
		{
			LobbyUserWidget->AddToViewport();
		}
	}
}

void ALobbyHUD::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	if (GetWorld() && LobbyUserWidgetClass) {
		TArray<UUserWidget*> FoundWidgets;
		UWidgetBlueprintLibrary::GetAllWidgetsOfClass(GetWorld(), FoundWidgets, UUserWidget::StaticClass());

		UE_LOG(LogTemp, Warning, TEXT("%s Warning"), *LobbyUserWidgetClass->GetClass()->GetFName().ToString());

		if (FoundWidgets.Num() > 0) {
			for (auto LobbyWidget:FoundWidgets) {
				if (LobbyWidget) {
					LobbyWidget->RemoveFromParent();
					LobbyWidget->RemoveFromViewport();
				}
			}
		}
	}
	Super::EndPlay(EndPlayReason);
}
