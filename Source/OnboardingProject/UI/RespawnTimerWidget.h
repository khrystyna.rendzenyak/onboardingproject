// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/TextBlock.h"
#include "RespawnTimerWidget.generated.h"


UCLASS()
class ONBOARDINGPROJECT_API URespawnTimerWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	void SetTimerText(int32 TimeInSeconds);

protected:
	UPROPERTY(meta = (BindWidget))
	UTextBlock* TimeToRespawnText;
};
