// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/Button.h"
#include "Components/TextBlock.h"
#include "Components/EditableText.h"
#include "Components/ComboBoxString.h"
#include "Blueprint/UserWidget.h"
#include "MenuWidget.generated.h"

/**
 * 
 */
UCLASS()
class ONBOARDINGPROJECT_API UMenuWidget : public UUserWidget
{
	GENERATED_BODY()

protected:
	virtual void NativeOnInitialized() override;

	UPROPERTY(meta = (BindWidget))
	UButton* CreateNewSaveButton;

	UPROPERTY(meta = (BindWidget))
	UButton* SelectGameSaveButton;

	UPROPERTY(meta = (BindWidget))
	UButton* StartGameButton;

	UPROPERTY(meta = (BindWidget))
	UButton* StartOnlineGameButton;

	UPROPERTY(meta = (BindWidget))
	UButton* LeaderboardButton;

	UPROPERTY(meta = (BindWidget))
	UEditableText* NameText;

	UPROPERTY(meta = (BindWidget))
	UComboBoxString* SavedNamesBox;

	UPROPERTY(meta = (BindWidget))
	UTextBlock* HelpText;

private:
	UFUNCTION()
	void OnSelectGameSave();

	UFUNCTION()
	void OnCreateNewSave();

	UFUNCTION()
	void OnStartGame();

	UFUNCTION()
	void OnStartOnlineGame();

	UFUNCTION()
	void OnLeaderboardOpen();

	UFUNCTION()
	void OnUserNameChanged(const FText& Text);

	UFUNCTION()
	void OnSelectedSave(FString SelectedItem, ESelectInfo::Type SelectionType);

	class UMultiplayerSessionSubsystem* MultiplayerSessionSubsystem;

	int32 NumPublicConnections{ 4 };
	FString MatchType{ TEXT("FreeForAll") };
};
