// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "LeaderboardWidget.generated.h"

class UVerticalBox;
class UButton;

UCLASS()
class ONBOARDINGPROJECT_API ULeaderboardWidget : public UUserWidget
{
	GENERATED_BODY()

protected:
	virtual void NativeOnInitialized() override;

	UPROPERTY(meta = (BindWidget))
	UVerticalBox* PlayerStatBox;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
	TSubclassOf<UUserWidget> UserStatisticWidget;

	UPROPERTY(meta = (BindWidget))
	UButton* MainMenuButton;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	int32 UserStatCount = 5;

private:
	UFUNCTION()
	void GoToMainMenu();

	void UpdateUserStat();
};
