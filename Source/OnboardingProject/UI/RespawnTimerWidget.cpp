// Fill out your copyright notice in the Description page of Project Settings.


#include "RespawnTimerWidget.h"
#include "Kismet/KismetStringLibrary.h"

void URespawnTimerWidget::SetTimerText(int32 TimeInSeconds)
{
	if (TimeToRespawnText)
	{
		TimeToRespawnText->SetText(FText::FromString(UKismetStringLibrary::TimeSecondsToString(TimeInSeconds).Mid(0, 5)));
	}
}