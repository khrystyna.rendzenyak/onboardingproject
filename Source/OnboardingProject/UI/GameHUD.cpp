// Fill out your copyright notice in the Description page of Project Settings.


#include "GameHUD.h"
#include "GameHUDWidget.h"

void AGameHUD::BeginPlay()
{
	Super::BeginPlay();

	if (HUDWidgetClass)
	{
		HUDWidget = CreateWidget<UUserWidget>(GetWorld(), HUDWidgetClass);
		if (HUDWidget)
		{
			HUDWidget->AddToViewport();

			GetWorldTimerManager().SetTimer(UpdateHUDHandle, this, &AGameHUD::UpdateHUD, UpdateHUDTimer, true);
		}
	}
}

void AGameHUD::UpdateHUD()
{
	if (HUDWidget)
	{
		UGameHUDWidget* GameHUDWidget = Cast<UGameHUDWidget>(HUDWidget);
		if (GameHUDWidget)
		{
			GameHUDWidget->UpdateHUD();
		}
	}
}