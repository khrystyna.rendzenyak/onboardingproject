// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/Button.h"
#include "LobbyUserWidget.generated.h"

/**
 * 
 */
UCLASS()
class ONBOARDINGPROJECT_API ULobbyUserWidget : public UUserWidget
{
	GENERATED_BODY()

protected:
	virtual void NativeOnInitialized() override;

	UPROPERTY(meta = (BindWidget))
	UButton* PlayGameButton;
private:
	UFUNCTION()
	void GoToGameplay();
};
