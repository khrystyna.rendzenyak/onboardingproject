// Fill out your copyright notice in the Description page of Project Settings.


#include "LobbyUserWidget.h"
#include "Kismet/GameplayStatics.h"	
#include "LobbyGameState.h"
#include "LobbyPlayerController.h"

void ULobbyUserWidget::NativeOnInitialized()
{
	Super::NativeOnInitialized();

	if (PlayGameButton)
	{
		PlayGameButton->OnClicked.AddDynamic(this, &ULobbyUserWidget::GoToGameplay);
	}
}

void ULobbyUserWidget::GoToGameplay()
{
	if (!GetWorld())
	{
		return;
	}
	ALobbyPlayerController* Lobby_PC = Cast<ALobbyPlayerController>(UGameplayStatics::GetPlayerController(GetWorld(), 0));
	if (!Lobby_PC)
	{
		return;
	}
	Lobby_PC->SetGameplayFlag(true);
}