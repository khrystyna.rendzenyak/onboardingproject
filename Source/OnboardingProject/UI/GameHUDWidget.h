// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/TextBlock.h"
#include "Components/ProgressBar.h"
#include "Components/Image.h"
#include "Blueprint/UserWidget.h"
#include "Animation/WidgetAnimation.h"
#include "GameHUDWidget.generated.h"

UCLASS()
class ONBOARDINGPROJECT_API UGameHUDWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	virtual void NativeOnInitialized() override;

	UPROPERTY(meta = (BindWidget))
	UProgressBar* HealthProgressBar;

	UPROPERTY(meta = (BindWidget))
	UTextBlock* HealthProgress;

	UPROPERTY(meta = (BindWidget))
	UTextBlock* Timer;

	UPROPERTY(meta = (BindWidget))
	UImage* DamageImage;

	//UPROPERTY(meta = (BindWidgetAnim))
	//UWidgetAnimation* DamageAnimation;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	float ZeroFloat = 0.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	int32 MaxPercent = 100;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	int32 LowHealthPercent = 20;

	UFUNCTION(BlueprintCallable, Category = "UI")
	FString GetHealthPercentText() const;

	UFUNCTION(BlueprintCallable, Category = "UI")
	int32 GetGameTime() const;

	float GetHealthPercent() const;
	void UpdateHUD();
};
