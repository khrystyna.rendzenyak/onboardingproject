// Fill out your copyright notice in the Description page of Project Settings.

#include "GameHUDWidget.h"
#include "HealthComponent.h"
#include "OnboardingProjectUtils.h"
#include "OnboardingProjectCharacter.h"
#include "OnboardingProjectGameState.h"
#include "Kismet/KismetStringLibrary.h"
#include "Kismet/GameplayStatics.h"

void UGameHUDWidget::NativeOnInitialized()
{
	Super::NativeOnInitialized();

	UpdateHUD();
}

void UGameHUDWidget::UpdateHUD()
{
	if (HealthProgress)
	{
		HealthProgress->SetText(FText::FromString(GetHealthPercentText()));
	}
	if (Timer)
	{
		Timer->SetText(FText::FromString(UKismetStringLibrary::TimeSecondsToString(GetGameTime()).Mid(0, 5)));
	}
	if (HealthProgressBar)
	{
		float HealthPercent = GetHealthPercent();
		int32 PlayerHPInteger = HealthPercent * MaxPercent;
		HealthProgressBar->SetPercent(HealthPercent);
	//	if ((PlayerHPInteger < LowHealthPercent) && DamageAnimation)
	//	{
	//		PlayAnimation(DamageAnimation);
	//	}
	}
}

float UGameHUDWidget::GetHealthPercent() const
{
	const APawn* Player = GetOwningPlayerPawn();
	if (!Player)
	{
		return ZeroFloat;
	}
	const UHealthComponent* HealthComponent = Cast<UHealthComponent>(Player->GetComponentByClass(UHealthComponent::StaticClass()));
	if (!HealthComponent)
	{
		return ZeroFloat;
	}
	
	return HealthComponent->GetHealthPercent(MaxPercent);
}

FString UGameHUDWidget::GetHealthPercentText() const
{
	float PlayerHP = GetHealthPercent();

	int32 PlayerHPInteger = PlayerHP * MaxPercent;
	return FString::FromInt(PlayerHPInteger) + "/" + FString::FromInt(MaxPercent) + " % ";
}

int32 UGameHUDWidget::GetGameTime() const
{
	const AOnboardingProjectCharacter* Player = Cast<AOnboardingProjectCharacter>(GetOwningPlayerPawn());
	const AOnboardingProjectGameState* GameState = Cast<AOnboardingProjectGameState>(GetWorld()->GetGameState());
	return Player ? GameState->GetPlayerStartTime(Player->GetPlayerController()) : 0;
}