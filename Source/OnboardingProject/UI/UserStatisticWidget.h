// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/TextBlock.h"
#include "UserStatisticWidget.generated.h"

UCLASS()
class ONBOARDINGPROJECT_API UUserStatisticWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	void SetPlayerStatistic(const FText& Number, const FText& Name, const FText& Time);

protected:
	UPROPERTY(meta = (BindWidget))
	UTextBlock* NumberText;

	UPROPERTY(meta = (BindWidget))
	UTextBlock* TimeText;

	UPROPERTY(meta = (BindWidget))
	UTextBlock* UserNameText;
};
