// Fill out your copyright notice in the Description page of Project Settings.


#include "UserStatisticWidget.h"

void UUserStatisticWidget::SetPlayerStatistic(const FText& Number, const FText& Name, const FText& Time)
{
	if (NumberText)
	{
		NumberText->SetText(Number);
	}
	if (TimeText)
	{
		TimeText->SetText(Time);
	}
	if (UserNameText)
	{
		UserNameText->SetText(Name);
	}
}
