// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "Blueprint/UserWidget.h"
#include "GameHUD.generated.h"

UCLASS()
class ONBOARDINGPROJECT_API AGameHUD : public AHUD
{
	GENERATED_BODY()

public:
	void UpdateHUD();

protected:
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
	TSubclassOf<UUserWidget> HUDWidgetClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	float UpdateHUDTimer = .2f;
private:
	UPROPERTY(VisibleAnywhere)
	UUserWidget* HUDWidget;

	FTimerHandle UpdateHUDHandle;
};
