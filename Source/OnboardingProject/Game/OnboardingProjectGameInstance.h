// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "OnboardingProjectSaveGame.h"
#include "OnboardingProjectGameInstance.generated.h"

class AOnboardingProjectGameMode;
class AOnboardingProjectCharacter;

//DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnLevelEnter)

UCLASS()
class ONBOARDINGPROJECT_API UOnboardingProjectGameInstance : public UGameInstance
{
	GENERATED_BODY()
	
public:
	static UOnboardingProjectGameInstance* Get();
	static bool IsValid();
	static bool IsInitialized();
	static bool IsShuttingDown();

	UOnboardingProjectGameInstance();

	virtual void Init() override;
	virtual void Shutdown() override;
	virtual void Reinitialize(bool SkipSaveGameManager = false);

	bool IsSaveGameManagerValid() const;
	UOnboardingProjectSaveGame& GetSaveGameManager() const;

private:
	static class UOnboardingProjectGameInstance* Instance;
	
	static bool bInitialized;
	static bool bFullyInitialized;
	static bool bShuttingDown;

	UPROPERTY(EditAnywhere, Category = "Game")
	UOnboardingProjectSaveGame* SaveGameManager;
};
