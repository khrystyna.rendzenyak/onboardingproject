// Fill out your copyright notice in the Description page of Project Settings.


#include "Game/OnboardingProjectGlobals.h"

static UOnboardingProjectGlobals* CachedGlobals = nullptr;

UOnboardingProjectGlobals& GetGlobals()
{
	if (CachedGlobals == nullptr || CachedGlobals->IsUnreachable())
	{
		CachedGlobals = NewObject<UOnboardingProjectGlobals>();
	}
	return *CachedGlobals;
}

void InitGlobals()
{
	CachedGlobals = NewObject<UOnboardingProjectGlobals>();
}

void DeleteGlobals()
{
	if (CachedGlobals && CachedGlobals != nullptr)
	{
		CachedGlobals->MarkPendingKill();
	}
	CachedGlobals = nullptr;
}

bool IsGlobalsValid()
{
	return (CachedGlobals != nullptr);
}
