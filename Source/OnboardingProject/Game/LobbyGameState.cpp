// Fill out your copyright notice in the Description page of Project Settings.


#include "LobbyGameState.h"
#include "LobbyGameMode.h"
#include "Kismet/GameplayStatics.h"	
#include "OnboardingProjectGlobals.h"
#include "Blueprint/WidgetBlueprintLibrary.h"
#include "Net/UnrealNetwork.h"

DEFINE_LOG_CATEGORY_STATIC(LogOPGameState, All, All);

void ALobbyGameState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(ALobbyGameState, isGameplayState);
}

void ALobbyGameState::SetGameplayFlag(bool GameplayState)
{
	isGameplayState = GameplayState;
	UE_LOG(LogOPGameState, Display, TEXT("SetGameplayFlag function"));
	OnRep_GameplayStateChanged();
}

bool ALobbyGameState::GetGameplayFlag() const
{
	return isGameplayState;
}

//void ALobbyGameState::OnRep_GameplayStateChanged()
//{
//	UE_LOG(LogOPGameState, Display, TEXT("OnRep_GameplayStateChanged function"));
//	
//	if (HasAuthority())
//	{
//		GetWorld()->ServerTravel(GetGlobals().GetMapsPath() + GetGlobals().StartupLevelName.ToString()+TEXT("?listen"));
//	}
//}

void ALobbyGameState::OnRep_GameplayStateChanged_Implementation()
{
	UE_LOG(LogOPGameState, Display, TEXT("OnRep_GameplayStateChanged function"));

	if (HasAuthority())
	{
		GetWorld()->ServerTravel(GetGlobals().GetMapsPath() + GetGlobals().StartupLevelName.ToString() + TEXT("?listen"));
	}
}

bool ALobbyGameState::OnRep_GameplayStateChanged_Validate()
{
	return true;
}
