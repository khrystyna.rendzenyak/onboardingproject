// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "LeaderboardGameMode.generated.h"

UCLASS()
class ONBOARDINGPROJECT_API ALeaderboardGameMode : public AGameModeBase
{
	GENERATED_BODY()

private:
	ALeaderboardGameMode();
	virtual void InitGame(const FString& MapName, const FString& Options, FString& ErrorMessage) override;
};
