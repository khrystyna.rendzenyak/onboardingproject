// Fill out your copyright notice in the Description page of Project Settings.


#include "LobbyGameMode.h"
#include "Kismet/GameplayStatics.h"	
#include "GameFramework/GameStateBase.h"
#include "Blueprint/WidgetBlueprintLibrary.h"
#include "LobbyHUD.h"
#include "Game/OnboardingProjectGameInstance.h"
#include "MultiplayerSessionSubsystem.h"
#include "OnboardingProjectGlobals.h"

ALobbyGameMode::ALobbyGameMode()
{
	HUDClass = ALobbyHUD::StaticClass();
}

void ALobbyGameMode::PostLogin(APlayerController* NewPlayer)
{
	Super::PostLogin(NewPlayer);
	
	bUseSeamlessTravel = true;
	UWorld* World = GetWorld();
	if (!World)
	{
		return;
	}

	if (GameState)
	{
		int32 NumberOfPlayers = GameState->PlayerArray.Num();
		if (NumberOfPlayers > 1)
		{
			World->ServerTravel(GetGlobals().GetMapsPath() + GetGlobals().StartupLevelName.ToString() + TEXT("?listen"));
		}
	}
}

void ALobbyGameMode::InitGame(const FString& MapName, const FString& Options, FString& ErrorMessage)
{
	Super::InitGame(MapName, Options, ErrorMessage);

	UOnboardingProjectGameInstance::Get()->Init();

	UWorld* World = GetWorld();
	if (!World)
	{
		return;
	}

	if (World && (World->GetNetMode() == ENetMode::NM_DedicatedServer|| World->GetNetMode() == ENetMode::NM_ListenServer))
	{
		auto MultiplayerSessionSubsystem = UOnboardingProjectGameInstance::Get()->GetSubsystem<UMultiplayerSessionSubsystem>();
		if (!MultiplayerSessionSubsystem)
		{
			return;
		}
		FString LobbyPath = GetGlobals().GetMapsPath() + GetGlobals().LobbyLevelName.ToString() + TEXT("?listen");
		MultiplayerSessionSubsystem->CreateSession(4, "FreeForAll", LobbyPath);
	}
}
