// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "OnboardingProjectGameMode.generated.h"

class UOnboardingProjectSaveGame;

UCLASS(minimalapi)
class AOnboardingProjectGameMode : public AGameMode
{
	GENERATED_BODY()

public:
	AOnboardingProjectGameMode();

	UFUNCTION()
	void SpawnHealthPickup();

	virtual void StartPlay() override;
	//virtual void Tick(float DeltaTime) override;

	virtual void InitGame(const FString& MapName, const FString& Options, FString& ErrorMessage) override;
	//virtual void PostLogin(APlayerController* NewPlayer) override;
	virtual void Logout(AController* Exiting) override;

private:
	UPROPERTY(EditAnywhere)
	TArray<AActor*> HealthPickupObjects;

	int32 ActiveHealthPickup = -1;
};



