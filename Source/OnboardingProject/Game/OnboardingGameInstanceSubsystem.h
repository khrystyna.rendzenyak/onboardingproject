// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Subsystems/GameInstanceSubsystem.h"
#include "OnboardingGameInstanceSubsystem.generated.h"

/**
 * 
 */
UCLASS()
class ONBOARDINGPROJECT_API UOnboardingGameInstanceSubsystem : public UGameInstanceSubsystem
{
	GENERATED_BODY()
	
};
