#include "OnboardingProjectUtils.h"
#include "Engine/Engine.h"
#include "Engine/World.h"
#include "OnboardingProjectGameMode.h"

AOnboardingProjectGameMode* OnboardingProjectUtils::GetGameMode(const UObject* WorldContextObject)
{
	UWorld* World = nullptr;
	if (GEngine && GEngine->GameViewport)
	{
		World = GEngine->GameViewport->GetWorld();
	}
	else if (GEngine)
	{
		World = GEngine->GetWorldFromContextObject(WorldContextObject);
	}

	return World ? World->GetAuthGameMode<AOnboardingProjectGameMode>() : nullptr;
}
