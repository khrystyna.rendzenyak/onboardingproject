// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "OnboardingProjectGlobals.generated.h"

/**
 * 
 */
UCLASS()
class ONBOARDINGPROJECT_API UOnboardingProjectGlobals : public UObject
{
	GENERATED_BODY()
public:
	UPROPERTY(VisibleAnywhere, Category = "Character")
	float MaxHealth = 100.0f;

	UPROPERTY(EditAnywhere, Category = "Pain Volume")
	float Damage = 1.0f;

	UPROPERTY(EditAnywhere, Category = "Pain Volume")
	float DamageMultiplier = 1.1f;

	UPROPERTY(EditAnywhere, Category = "Pain Volume")
	float DamageTimer = 1.0f;

	UPROPERTY(EditAnywhere, Category = "Pain Volume")
	float IncreaseDamageTimer = 5.0f;

	UPROPERTY(EditAnywhere, Category = "Speed Pickup")
	float SpeedMultiplier = 1.5f;

	UPROPERTY(EditAnywhere, Category = "Speed Pickup")
	float SpeedTime = 5.f;

	UPROPERTY(EditAnywhere, Category = "Speed Pickup")
	float RespawnTime = 15.f;

	UPROPERTY(EditDefaultsOnly, Category = "Game")
	FName StartupLevelName = "Gameplay";

	UPROPERTY(EditDefaultsOnly, Category = "Game")
	FName LeaderboardLevelName = "Leaderboard";

	UPROPERTY(EditDefaultsOnly, Category = "Game")
	FName MenuLevelName = "MainMenu";

	UPROPERTY(EditDefaultsOnly, Category = "Game")
	FName LobbyLevelName = "Lobby";
	
	UFUNCTION()
	FString GetMapsPath() const { return "/Game/Maps/"; }
};

UOnboardingProjectGlobals& GetGlobals(); 
void InitGlobals();
void DeleteGlobals();
bool IsGlobalsValid();