// Fill out your copyright notice in the Description page of Project Settings.


#include "MainMenuGameMode.h"
#include "MenuPlayerController.h"
#include "UI/MainMenuHUD.h"
#include "Game/OnboardingProjectGameInstance.h"

AMainMenuGameMode::AMainMenuGameMode()
{
	PlayerControllerClass = AMenuPlayerController::StaticClass();
	HUDClass = AMainMenuHUD::StaticClass();
}

void AMainMenuGameMode::InitGame(const FString& MapName, const FString& Options, FString& ErrorMessage)
{
	Super::InitGame(MapName, Options, ErrorMessage);

	UOnboardingProjectGameInstance::Get()->Init();
}
