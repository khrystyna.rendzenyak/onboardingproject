// Copyright Epic Games, Inc. All Rights Reserved.

#include "OnboardingProjectGameMode.h"
#include "OnboardingProjectCharacter.h"
#include "HealthPickup.h"
#include "UObject/ConstructorHelpers.h"
#include "Math/UnrealMathUtility.h"
#include "GameHUD.h"
#include "Kismet/GameplayStatics.h"
#include "OnboardingProjectGlobals.h"
#include "Blueprint/WidgetBlueprintLibrary.h"
#include "Game/OnboardingProjectGameInstance.h"
#include "Game/OnboardingProjectGameState.h"
#include <MultiplierSession/Public/MultiplayerSessionSubsystem.h>

AOnboardingProjectGameMode::AOnboardingProjectGameMode()
{
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprints/ThirdPersonCharacter"));
	PlayerControllerClass = AGameplayPlayerController::StaticClass();
	HUDClass = AGameHUD::StaticClass();
}

void AOnboardingProjectGameMode::StartPlay()
{
	Super::StartPlay();

	TSubclassOf<AHealthPickup> ClassToFind = AHealthPickup::StaticClass();
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ClassToFind, HealthPickupObjects);
	if (HealthPickupObjects.Num() <= 0)
		return;
	SpawnHealthPickup();
}

//void AOnboardingProjectGameMode::PostLogin(APlayerController* NewPlayer)
//{
//	Super::PostLogin(NewPlayer);
//	UOnboardingProjectGameInstance::Get()->AddLocalPlayer(NewPlayer->GetLocalPlayer(), UOnboardingProjectGameInstance::Get()->GetNumLocalPlayers());
//}

void AOnboardingProjectGameMode::SpawnHealthPickup()
{
	if (HealthPickupObjects.Num() == 1 && ActiveHealthPickup != -1)
	{
		return;
	}

	AGameHUD* GameHUDClass = Cast<AGameHUD>(HUDClass);
	if (GameHUDClass)
	{
		GameHUDClass->UpdateHUD();
	}

	int32 previousNumb = ActiveHealthPickup;
	ActiveHealthPickup = FMath::RandRange(0, HealthPickupObjects.Num()-1);

	if (ActiveHealthPickup == previousNumb)
	{
		if (ActiveHealthPickup == 0)
		{
			ActiveHealthPickup++;
		}
		else
		{
			ActiveHealthPickup--;
		}
	}

	AHealthPickup* healthPickup = Cast<AHealthPickup>(HealthPickupObjects[ActiveHealthPickup]);
	if (healthPickup)
	{
		healthPickup->Respawn();
	}
}

void AOnboardingProjectGameMode::InitGame(const FString& MapName, const FString& Options, FString& ErrorMessage)
{
	Super::InitGame(MapName, Options, ErrorMessage);

	UOnboardingProjectGameInstance::Get()->Init();
}

void AOnboardingProjectGameMode::Logout(AController* Exiting)
{
	auto MultiplayerSessionSubsystem = UOnboardingProjectGameInstance::Get()->GetSubsystem<UMultiplayerSessionSubsystem>();
	if (MultiplayerSessionSubsystem)
	{
		UE_LOG(LogTemp, Warning, TEXT("MultiplayerSessionSubsystem valid"))
		MultiplayerSessionSubsystem->DestroySession();
	}
	AOnboardingProjectCharacter* CharacterLeaving = Cast<AOnboardingProjectCharacter>(Exiting->GetPawn());
	if (CharacterLeaving)
	{
		CharacterLeaving->Elim(true);
	}
}
