// Fill out your copyright notice in the Description page of Project Settings.


#include "OnboardingProjectGameInstance.h"
#include "MultiplayerSessionSubsystem.h"
#include "OnboardingProjectGlobals.h"
#include <SocketSubsystem.h>
#include <IPAddress.h>

UOnboardingProjectGameInstance* UOnboardingProjectGameInstance::Instance = nullptr;
bool UOnboardingProjectGameInstance::bInitialized = false;
bool UOnboardingProjectGameInstance::bFullyInitialized = false;
bool UOnboardingProjectGameInstance::bShuttingDown = false;
UOnboardingProjectGameInstance* UOnboardingProjectGameInstance::Get()
{
	return Instance;
}

bool UOnboardingProjectGameInstance::IsValid() 
{
	return (Instance != nullptr);
}

bool UOnboardingProjectGameInstance::IsInitialized()
{
	if (Instance)
	{
		return bFullyInitialized;
	}
	return false;
}

bool UOnboardingProjectGameInstance::IsShuttingDown()
{
	if (Instance)
	{
		return bShuttingDown;
	}
	return false;
}


UOnboardingProjectGameInstance::UOnboardingProjectGameInstance()
{
	bool canBind = false; 
	
	//TSharedRef<FInternetAddr> localIp = ISocketSubsystem::Get(PLATFORM_SOCKETSUBSYSTEM)->GetLocalHostAddr(*GLog, canBind);
	//currentIPAddress = localIp->ToString(false);
}

void UOnboardingProjectGameInstance::Init()
{
	Super::Init();

	Instance = this;
	bInitialized = true;
	bShuttingDown = false;
	if (GetWorld() && (GetWorld()->GetNetMode() != ENetMode::NM_DedicatedServer))
	{
		SaveGameManager = NewObject<UOnboardingProjectSaveGame>();
		SaveGameManager->Init();
	}
	InitGlobals();

	bFullyInitialized = true;
}

void UOnboardingProjectGameInstance::Shutdown()
{
	bFullyInitialized = false;
	Super::Shutdown();
	bShuttingDown = true;

	SaveGameManager->Shutdown();
	DeleteGlobals();

	Instance = nullptr;
	bInitialized = false;
}

void UOnboardingProjectGameInstance::Reinitialize(bool SkipSaveGameManager)
{
}

bool UOnboardingProjectGameInstance::IsSaveGameManagerValid() const
{
	return SaveGameManager != nullptr;
}

UOnboardingProjectSaveGame& UOnboardingProjectGameInstance::GetSaveGameManager() const
{
	check(SaveGameManager)
	return *SaveGameManager;
}

