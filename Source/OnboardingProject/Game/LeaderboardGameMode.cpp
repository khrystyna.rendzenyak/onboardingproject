// Fill out your copyright notice in the Description page of Project Settings.


#include "LeaderboardGameMode.h"
#include "MenuPlayerController.h"
#include "Kismet/GameplayStatics.h"
#include "LeaderboardHUD.h"
#include "Game/OnboardingProjectGameInstance.h"

ALeaderboardGameMode::ALeaderboardGameMode()
{
	PlayerControllerClass = AMenuPlayerController::StaticClass();
	HUDClass = ALeaderboardHUD::StaticClass();
}


void ALeaderboardGameMode::InitGame(const FString& MapName, const FString& Options, FString& ErrorMessage)
{
	Super::InitGame(MapName, Options, ErrorMessage);

	UOnboardingProjectGameInstance::Get()->Init();
}