// Fill out your copyright notice in the Description page of Project Settings.


#include "HealthComponent.h"
#include "OnboardingProjectGlobals.h"
#include "OnboardingProjectUtils.h"
#include "OnboardingProjectCharacter.h"
#include "Net/UnrealNetwork.h"

DECLARE_CYCLE_STAT(TEXT("OnTakeDamage"), STAT_OnTakeDamage, STATGROUP_HEALTH)
DECLARE_DWORD_ACCUMULATOR_STAT(TEXT("HealthChangedCounter"), STAT_HealthChanged, STATGROUP_HEALTH)

UHealthComponent::UHealthComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
}

float UHealthComponent::GetHealthPercent(int32 MaxHealth) const
{
	return Health / MaxHealth;
}

void UHealthComponent::SetHealth(float NewHealth)
{
	Health = FMath::Clamp(NewHealth, 0.0f, GetGlobals().MaxHealth);
	OnRep_HealthChanged();
}

void UHealthComponent::OnRep_HealthChanged()
{
	OnHealthChanged.Broadcast(Health);
	if (IsDead())
	{
		OnDeath.Broadcast();
	}
}

bool UHealthComponent::TryToAddHealth(float HeathAmount)
{
	if (IsDead() || IsHealthFull())
		return false;

	INC_DWORD_STAT(STAT_HealthChanged)
	SetHealth(Health + HeathAmount);
	return true;
}

bool UHealthComponent::IsHealthFull() const
{
	return FMath::IsNearlyEqual(Health, GetGlobals().MaxHealth);
}

void UHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	SetHealth(GetGlobals().MaxHealth);
	
	AActor* ComponentOwner = GetOwner();
	if (ComponentOwner)
	{
		if (ComponentOwner->HasAuthority())
		{
			ComponentOwner->OnTakeAnyDamage.AddDynamic(this, &UHealthComponent::OnTakeAnyDamage);
		}
	}
}

void UHealthComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME_CONDITION(UHealthComponent, Health, COND_OwnerOnly);
}

void UHealthComponent::OnTakeAnyDamage(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser)
{
	if (Damage <= ZeroDamage || IsDead())
	{
		return;
	}

	SCOPE_CYCLE_COUNTER(STAT_OnTakeDamage)
	SetHealth(Health-Damage);
}