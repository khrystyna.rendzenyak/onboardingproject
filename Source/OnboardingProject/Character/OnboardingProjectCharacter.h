// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "HealthComponent.h"
#include "Player/GameplayPlayerController.h"
#include "Interfaces/OnlineSessionInterface.h"
#include "OnboardingProjectCharacter.generated.h"

class UHealthComponent;

UCLASS(config=Game)
class AOnboardingProjectCharacter : public ACharacter
{
	GENERATED_BODY()

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCamera;

public:
	AOnboardingProjectCharacter();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseTurnRate;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseLookUpRate;

	FORCEINLINE class USpringArmComponent*	GetCameraBoom()		const { return CameraBoom; }
	FORCEINLINE class UCameraComponent*		GetFollowCamera()	const { return FollowCamera; }
	FORCEINLINE bool						IsDead()			const { return HealthComponent->IsDead(); }
	FORCEINLINE	FString						GetIPAddress()		const { return IPAddress; }
	FORCEINLINE	FString						GetUserName()		const { return UserName; }

	AGameplayPlayerController* GetPlayerController() const { return PlayerController.Get(); }
	virtual void PossessedBy(AController* NewController) override;
	virtual void UnPossessed() override;
	void Elim(bool bPlayerLeftGame);
protected:
	UPROPERTY()
	UHealthComponent* HealthComponent;

	void OnResetVR();
	void MoveForward(float Value);
	void MoveRight(float Value);
	void TurnAtRate(float Rate);
	void LookUpAtRate(float Rate);
	void TouchStarted(ETouchIndex::Type FingerIndex, FVector Location);
	void TouchStopped(ETouchIndex::Type FingerIndex, FVector Location);

	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	virtual void BeginPlay() override;
private:

	UPROPERTY()
	FString UserName = "Test";

	UPROPERTY()
	FString IPAddress;

	UFUNCTION()
	void OnDeath();

	UFUNCTION()
	void CheckHealth(float health);
	
	UFUNCTION(Server, Reliable)
	void ServerLeaveGame();
	void ServerLeaveGame_Implementation();

	UFUNCTION(NetMulticast, Reliable)
	void MulticastElim(bool bPlayerLeftGame);
	void MulticastElim_Implementation(bool bPlayerLeftGame);
	FTimerHandle GameTimerHandle;

	TWeakObjectPtr<AGameplayPlayerController> PlayerController;

	bool bElimmed = false;

	FTimerHandle ElimTimer;

	UPROPERTY(EditDefaultsOnly)
	float ElimDelay = 1.f;

	void ElimTimerFinished();
};

