// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "HealthComponent.generated.h"

DECLARE_MULTICAST_DELEGATE(FonDeath)
DECLARE_MULTICAST_DELEGATE_OneParam(FonHealthChanged, float)
DECLARE_STATS_GROUP(TEXT("HEALTH_Game"), STATGROUP_HEALTH, STATCAT_Advance)

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class ONBOARDINGPROJECT_API UHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UHealthComponent();

	UFUNCTION()
	void OnTakeAnyDamage(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser);
		
	UFUNCTION(BlueprintCallable)
	bool IsDead() const { return Health <= 0.0f; }

	UFUNCTION(BlueprintCallable)
	float GetHealthPercent(int32 MaxHEalth) const;
	
	float GetHealth() const { return Health; }

	UFUNCTION()
	void SetHealth(float NewHealth);

	UFUNCTION()
	void OnRep_HealthChanged();

	bool TryToAddHealth(float HeathAmount);
	bool IsHealthFull() const;

	FonDeath OnDeath;
	FonHealthChanged OnHealthChanged;

protected:
	virtual void BeginPlay() override;
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
private:
	UPROPERTY(ReplicatedUsing=OnRep_HealthChanged)
	float Health = 0.0f;

	const float ZeroDamage = 0.0f;
};
