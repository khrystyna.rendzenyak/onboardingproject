// Copyright Epic Games, Inc. All Rights Reserved.

#include "OnboardingProjectCharacter.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetSystemLibrary.h"
#include "OnboardingProjectUtils.h"
#include "OnboardingProjectGameInstance.h"
#include "OnboardingProjectGlobals.h"
#include "OnboardingProjectGameMode.h"
#include "OnboardingProjectPlayerState.h"
#include "Game/OnboardingProjectGameState.h"
#include "Net/UnrealNetwork.h"
#include "OnlineSubsystem.h"
#include "OnlineSessionSettings.h"
#include "Game/OnboardingProjectGlobals.h"

AOnboardingProjectCharacter::AOnboardingProjectCharacter()
{
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	GetCharacterMovement()->bOrientRotationToMovement = true; 
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f);
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;

	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 300.0f; 
	CameraBoom->bUsePawnControlRotation = true; 

	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); 
	FollowCamera->bUsePawnControlRotation = false;

	HealthComponent = CreateDefaultSubobject<UHealthComponent>(TEXT("HealthComponent"));
	HealthComponent->SetIsReplicated(true);
}

void AOnboardingProjectCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	check(PlayerInputComponent);
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	PlayerInputComponent->BindAxis("MoveForward", this, &AOnboardingProjectCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AOnboardingProjectCharacter::MoveRight);

	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &AOnboardingProjectCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &AOnboardingProjectCharacter::LookUpAtRate);

	PlayerInputComponent->BindTouch(IE_Pressed, this, &AOnboardingProjectCharacter::TouchStarted);
	PlayerInputComponent->BindTouch(IE_Released, this, &AOnboardingProjectCharacter::TouchStopped);

	PlayerInputComponent->BindAction("ResetVR", IE_Pressed, this, &AOnboardingProjectCharacter::OnResetVR);
}

void AOnboardingProjectCharacter::BeginPlay()
{
	Super::BeginPlay();

	if (HealthComponent)
	{
		HealthComponent->OnDeath.AddUObject(this, &AOnboardingProjectCharacter::OnDeath);
		HealthComponent->OnHealthChanged.AddUObject(this, &AOnboardingProjectCharacter::CheckHealth);
	}
}

void AOnboardingProjectCharacter::PossessedBy(AController* NewController)
{
	Super::PossessedBy(NewController);
	
	PlayerController = Cast<AGameplayPlayerController>(NewController);
}

void AOnboardingProjectCharacter::UnPossessed()
{
	PlayerController = nullptr;

	Super::UnPossessed();
}

void AOnboardingProjectCharacter::OnResetVR()
{
	UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
}

void AOnboardingProjectCharacter::TouchStarted(ETouchIndex::Type FingerIndex, FVector Location)
{
	Jump();
}

void AOnboardingProjectCharacter::TouchStopped(ETouchIndex::Type FingerIndex, FVector Location)
{
	StopJumping();
}

void AOnboardingProjectCharacter::TurnAtRate(float Rate)
{
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void AOnboardingProjectCharacter::LookUpAtRate(float Rate)
{
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void AOnboardingProjectCharacter::MoveForward(float Value)
{
	if ((Controller != nullptr) && (Value != 0.0f))
	{
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);
	}
}

void AOnboardingProjectCharacter::MoveRight(float Value)
{
	if ( (Controller != nullptr) && (Value != 0.0f) )
	{
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);
	
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		AddMovementInput(Direction, Value);
	}
}

void AOnboardingProjectCharacter::CheckHealth(float health)
{
	if (health <= 0)
	{
		OnDeath();
	}
}

void AOnboardingProjectCharacter::OnDeath()
{
	if (HealthComponent->GetHealth() > 0)
	{
		return;
	}
	UWorld* World = GetWorld();
	if (!World)
	{
		return;
	}
	//ServerLeaveGame_Implementation();
	if (World && (World->GetNetMode() != ENetMode::NM_DedicatedServer))
	{
		UOnboardingProjectSaveGame& SaveGameManager = UOnboardingProjectGameInstance::Get()->GetSaveGameManager();
		SaveGameManager.SetUserData(GetWorld()->GetGameState()->GetPlayerStartTime(PlayerController.Get()));
		SaveGameManager.SaveGame();
		FString MapPathToTravel = GetGlobals().GetMapsPath() + GetGlobals().LeaderboardLevelName.ToString();
		GetWorld()->GetFirstPlayerController()->ClientTravel(MapPathToTravel, TRAVEL_Absolute);
	}
}

void AOnboardingProjectCharacter::ServerLeaveGame_Implementation()
{
	UWorld* World = GetWorld();
	if (!World)
	{
		return;
	}
	AOnboardingProjectGameMode* CurrentGameMode = OnboardingProjectUtils::GetGameMode(World);
	CurrentGameMode == nullptr ? World->GetAuthGameMode<AOnboardingProjectGameMode>() : CurrentGameMode;
	if (CurrentGameMode)
	{
		CurrentGameMode->Logout(PlayerController.Get());
	}
}

void AOnboardingProjectCharacter::Elim(bool bPlayerLeftGame)
{
	if (!HasAuthority())
	{
		MulticastElim(bPlayerLeftGame);
	}
}

void AOnboardingProjectCharacter::MulticastElim_Implementation(bool bPlayerLeftGame)
{
	GetCharacterMovement()->DisableMovement();
	
	GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	GetMesh()->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	if (!HasAuthority())
	{
		GetWorldTimerManager().SetTimer(
			ElimTimer,
			this,
			&AOnboardingProjectCharacter::ElimTimerFinished,
			ElimDelay
		);
	}
}

void AOnboardingProjectCharacter::ElimTimerFinished()
{
	UE_LOG(LogTemp, Warning, TEXT("On Player Left Game"))
	
	if (IsLocallyControlled())
	{
		FString MapPathToTravel = GetGlobals().GetMapsPath() + GetGlobals().LeaderboardLevelName.ToString();
		GetWorld()->GetFirstPlayerController()->ClientTravel(MapPathToTravel, TRAVEL_Absolute);
	}
}